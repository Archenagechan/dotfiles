cp ~/.config/dunst/QWE/* -r ~/.config/dunst/ 
cp ~/.config/rofi/QWE/* -r ~/.config/rofi
cp ~/.config/polybar/QWE/* -r ~/.config/polybar
cp ~/.config/i3/QWE/* -r ~/.config/i3/
cp /home/crystal/.config/gtk-3.0/QWE/* -r /home/crystal/.config/gtk-3.0/  
i3 restart
pkill dunst
notify-send "Dark mode has been enabled, welcome to the darkside , enjoy"
