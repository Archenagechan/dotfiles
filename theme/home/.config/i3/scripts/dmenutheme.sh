#!/bin/bash
MENU=$(echo -e "QWE-Dark\nGeorge\nPink" | dmenu -i -fn 'FiraCode' -h '40' -i -l '5' -c -bw 3 -nb '#FBBFBF' -nf '#3944A8' -sf '#ACFEFB' -sb '#3944A8')
            case "$MENU" in
				## Colors
				QWE-Dark) ~/.config/i3/scripts/qwe.sh && i3 restart ;;
				George) ~/.config/i3/scripts/george.sh && i3 restart ;;
				Pink) ~/.config/i3/scripts/pink.sh && i3 restart ;;
            esac
