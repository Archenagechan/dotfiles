#/bin/sh
outputs=$(pacmd list-sources | grep name: | awk '{print $2}'| sed 's|<||;s|>||' | tee /tmp/outputsourcesffmpeg)
if [[ ! $(grep sink /tmp/outputsourcesffmpeg) ]];then
    kitty -c ~/.config/kitty/Pink/kitty.conf -e ffmpeg -y -f x11grab -framerate 60 -s "$(xdpyinfo | awk '/dimensions/ {print $2;}')" -i $(echo $DISPLAY) -f pulse -i $(grep output /tmp/outputsourcesffmpeg) -r 30 -c:v h264 -crf 0 -preset ultrafast -c:a aac "$HOME/Videos/recordings/$(date | sed 's|\s|-|g;s|--|-|g').mp4"; notify-send 'Recording finished! Video file written to disk' -a 'ffmpeg screen recorder' -u critical -t 5000
else
    kitty -c ~/.config/kitty/Pink/kitty.conf -e ffmpeg -y -f x11grab -framerate 60 -s "$(xdpyinfo | awk '/dimensions/ {print $2;}')" -i $(echo $DISPLAY) -f pulse -i $(grep sink /tmp/outputsourcesffmpeg) -r 30 -c:v h264 -crf 0 -preset ultrafast -c:a aac "$HOME/Videos/recordings/$(date | sed 's|\s|-|g;s|--|-|g').mp4"; notify-send 'Recording finished! Video file written to disk' -a 'ffmpeg screen recorder' -u critical -t 5000
fi