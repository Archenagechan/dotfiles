cp ~/.config/dunst/Pink/* -r ~/.config/dunst/ 
cp ~/.config/rofi/Pink/* -r ~/.config/rofi
cp ~/.config/polybar/Pink/* -r ~/.config/polybar
cp ~/.config/i3/Pink/* -r ~/.config/i3/
cp /home/crystal/.config/gtk-3.0/Pink/* -r /home/crystal/.config/gtk-3.0/ 
i3 restart
pkill dunst
notify-send "Pink mode has been enabled,sit back ,relax , and be comfy"
