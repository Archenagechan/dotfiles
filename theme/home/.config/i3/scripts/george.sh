cp ~/.config/dunst/George/* -r ~/.config/dunst/ 
cp ~/.config/polybar/George/* -r ~/.config/polybar/
cp ~/.config/rofi/George/* -r ~/.config/rofi/ 
cp ~/.config/i3/George/* -r ~/.config/i3/  
cp /home/crystal/.config/gtk-3.0/George/* -r /home/crystal/.config/gtk-3.0/ 
i3 restart
pkill dunst 
notify-send "George theme had been successfully applied, Enjoy ;)"
