# dotfiles

**Dependancies :** kitty rofi picom(ibhangwan fork) polybar i3-gaps-rounded-git dunst mpd ncmpcpp mpv qrencode cava ytfzf feh i3lock and dmenu(https://github.com/Stardust-kyun/dmenu) and bibata cursor pack


You may need to change some monitor variables (just search for eDP1 in i3 configs and polybar configs and replace them with your monitor)

Demo :
https://cdn.discordapp.com/attachments/635625917623828520/884398236075450408/lun.-06-sept.-2021-121255-CET.mp4


Credits : big thanks to 
Stardust-kyun(https://github.com/Stardust-kyun) for the dmenu fork and dmenu script
LetsHaveKiddos(https://github.com/LetsHaveKiddos/) for the base config of polybar, i3 dunst and rofi
cr0nus (https://github.com/AakashSharma7269) and LukeSmith On Crack (https://github.com/AakashSharma7269) for the help with troubleshooting and making scripts
adi1090x (https://github.com/adi1090x) for making archcraft which was the inspiration for this rice
moonpeace#6666 for the help with gitlab
And a big thanks to the archcraft discord server (https://discord.gg/TNXaq7y5EY)
